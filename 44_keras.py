#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 27 09:05:45 2023

@author: steca
"""

# Allows debug on spyder in my spyder-cf environment
import collections
collections.Callable = collections.abc.Callable


#%% Import moduli
import numpy as np
from matplotlib import pyplot as plt

import time

import tensorflow as tf
import keras


from sklearn import utils
from sklearn import model_selection 
from sklearn import preprocessing
from sklearn import metrics

from sklearn.model_selection import train_test_split, ParameterGrid

# K-fold validation
from sklearn.model_selection import RepeatedStratifiedKFold

# PCA
from sklearn.decomposition import PCA

# Scoring
# from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
# from sklearn.metrics import roc_curve, auc
from sklearn import metrics



#%%
thrEV = .95     # Soglia Explained Variance per pruning componenti
testEvts = None    # Numero di eventi del TeS da usare per il test (None = tutti)

T = time.time()

#%% Load dati
dati = np.load("Dataset/dati.npy")
dati = utils.shuffle(dati)



X = dati[:,1:]
y = dati[:,0]


#%% Costruisco Train Set e Test Set
X_train, X_test, y_train, y_test = train_test_split(X, y ,test_size = 0.25, random_state=1, shuffle = True) 
print(X_train.shape)




#%% Nuova prova scaling
def myCustomScalingTraining(_X):
    
    outX = np.zeros_like(_X)
    
    nRow = _X.shape[0]
    nCol = _X.shape[1]
    
    myMean = np.mean(_X, axis = 0)
    myStd = np.std(_X, axis = 0)
    
    lstSoloPos = []
    
    # print(myMean.shape, myStd.shape)
    # print(myMean, myStd)
    
    for i in range(nCol):
        if np.sum(_X[:,i] >= 0) == nRow:
            #print(f"La feature {i} ha solo valori positivi")
            
            outX[:,i] = (_X[:,i] - (myMean[i]-1) ) / myStd[i]
            lstSoloPos.append(i)
        else:
            outX[:,i] = (_X[:,i] - myMean[i] ) / myStd[i]
            
    return outX, myMean, myStd, lstSoloPos


def myCustomScalingTest(_X, _mean, _std, lstPos):
    
    outX = np.zeros_like(_X)

    nRow = _X.shape[0]
    nCol = _X.shape[1]

    
    myMean = _mean
    myStd = _std
    
    for i in range(nCol):
        if i in lstPos:
            outX[:,i] = (_X[:,i] - (myMean[i]-1) ) / myStd[i]
        else:
            outX[:,i] = (_X[:,i] - myMean[i] ) / myStd[i]

    return outX


# Una delle due modifiche
#X_train_scaled, myMean, myStd, lstSoloPos = myCustomScalingTraining(X_train)




#%% Scaling, PCA e Pruning

# Applico scaling MinMax
scaler = preprocessing.MinMaxScaler()
# =============================================================================
#scaler = preprocessing.StandardScaler()
scaler.fit(X_train)
X_train_scaled = scaler.transform(X_train)
# 
# =============================================================================



# Applico la PCA
pca = PCA()
pca.fit(X_train_scaled)
X_train_pca = pca.transform(X_train_scaled)


# Estraggo le Explained Variances
EV = pca.explained_variance_ratio_
cumEV = np.cumsum(EV)


# Stabilisco che componenti tenere per il pruning
# Le componenti sono già ordinate
idxLastFeature = np.sum(cumEV <= thrEV)
print(f"Per avere il {thrEV * 100} % di Explaied Variance, sono necessarie {idxLastFeature} features ")


# Pruno tenendo solo le prime componenti più informative
X_train_pruned = X_train_pca[:,:idxLastFeature]     # Pruno
y_train_pruned = y_train                            # Per uniformità di notazione




# Estraggo info su Test Set
X_test_scaled = scaler.transform(X_test)        # Riscalo

# Nuovo riscalamento
#X_test_scaled = myCustomScalingTest(X_test,  myMean, myStd, lstSoloPos)

X_test_pca = pca.transform(X_test_scaled)       # Trasformo PCA 

X_test_pruned = X_test_pca[:,:idxLastFeature]   # Pruno
y_test_pruned = y_test                          # Per uniformità di notazione


print(X_train_pruned.shape)


#%% ================= Modello Keras =================

# Definisco il modello
model = keras.Sequential()

# Aggiungo un layer di input
model.add(keras.Input(shape=(idxLastFeature,)))

# First layer
nNeuron = 2**11 #300

myIni = keras.initializers.RandomNormal(mean=0.0, stddev=0.1, seed=None)

model.add(keras.layers.Dense(nNeuron, activation="tanh", kernel_initializer = myIni))
model.add(keras.layers.Dropout(0.1))


# Mid layer
myIni2 = keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=None)

for i in range(4):
    model.add(keras.layers.Dense(nNeuron, activation="tanh", kernel_initializer = myIni2))
    model.add(keras.layers.Dropout(0.1))


# Out layer
myIni3 = keras.initializers.RandomNormal(mean=0.0, stddev=0.001, seed=None)
model.add(keras.layers.Dense(1, activation="sigmoid", kernel_initializer = myIni3))





# Ottimizzatore
myOpt = keras.optimizers.Adam(learning_rate = .1,  )
myOpt = keras.optimizers.SGD(lr=0.0001, decay=1e-6, momentum=0.9, nesterov=True)
#myOpt = keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)



# Compilo il modello
model.compile(myOpt, loss="binary_crossentropy", metrics=["AUC", "binary_accuracy"])


print( model.summary() )



# Fitto
history = model.fit(X_train_pruned, y_train, epochs = 1000, batch_size = int(y_train.shape[0]/100),
		validation_data = (X_test_pruned, y_test),)#validation_split=0.1,)


y_pred = model.predict(X_test_pruned[:testEvts,:])




#%% Performance
confusionMatrix =   metrics.confusion_matrix(y_test[:testEvts], y_pred)
accuracy        =   metrics.accuracy_score(y_test[:testEvts], y_pred)
precision       =   metrics.precision_score(y_test[:testEvts], y_pred)
recall          =   metrics.recall_score(y_test[:testEvts], y_pred)
f1              =   metrics.f1_score(y_test[:testEvts], y_pred)


print(confusionMatrix)
print(accuracy)
print(precision)
print(recall)
print(f1)


print(f"Fine di tutto: tempo passato: {time.time() - T}")
