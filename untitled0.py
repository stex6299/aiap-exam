# -*- coding: utf-8 -*-
"""
Created on Sat Feb 18 18:27:44 2023

@author: steca
"""

#%% Import moduli
import numpy as np
from matplotlib import pyplot as plt

import time, os, re, sys

from sklearn import utils
from sklearn import model_selection 
from sklearn import preprocessing
from sklearn import metrics

from sklearn.model_selection import train_test_split

# K-fold validation
from sklearn.model_selection import RepeatedStratifiedKFold

# PCA
from sklearn.decomposition import PCA

# Scoring
from sklearn.metrics import confusion_matrix, accuracy_score

# Classificatori
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier



#%% Settings

thrEV = .99     # Soglia Explained Variance per pruning componenti
drawPlot = True

if drawPlot:
    %matplotlib inline

#%% Load dei dati
os.chdir(r"G:\Corsi\AIAP - Artificial Intelligence for Astrophysical Problems\Esame\Repo")
dati = np.load("Dataset/dati.npy")
dati.shape

#%% Separo i dati

"""
Poichè nei dati appare per la prima metà la classe 0 e per la seconda la classe 1,
opero un reshuffling dei dati

Uso la convenzione X maiuscola per le matrici, y minuscola
Cosi sono in accordo
"""
#dati = utils.shuffle(dati)

X = dati[:,1:]
y = dati[:,0]




#%% Costruisco Train Set e Test Set
X_train, X_test, y_train, y_test = train_test_split(X, y ,test_size = 0.25, random_state=1, shuffle = True) 

#%% Scaling, PCA e Pruning

# Applico scaling MinMax
scaler = preprocessing.MinMaxScaler()
scaler.fit(X_train)
X_train_scaled = scaler.transform(X_train)


# Applico la PCA
pca = PCA()
pca.fit(X_train_scaled)
X_train_pca = pca.transform(X_train_scaled)


# Estraggo le Explained Variances
EV = pca.explained_variance_ratio_
cumEV = np.cumsum(EV)


# Stabilisco che componenti tenere per il pruning
# Le componenti sono già ordinate
idxLastFeature = np.sum(cumEV <= thrEV)
print(f"Per avere il {thrEV * 100} % di Explaied Variance, sono necessarie {idxLastFeature} features ")


# Pruno tenendo solo le prime componenti più informative
X_train_pruned = X_train_pca[:,:idxLastFeature]     # Pruno
y_train_pruned = y_train                            # Per uniformità di notazione




# Estraggo info su Test Set
X_test_scaled = scaler.transform(X_test)        # Riscalo
X_test_pca = pca.transform(X_test_scaled)       # Trasformo PCA 

X_test_pruned = X_test_pca[:,:idxLastFeature]   # Pruno
y_test_pruned = y_test                          # Per uniformità di notazione

#%% Verifico di aver shufflato [Copiato da 01]

# Indice degli eventi (0, 1, 2....)
timeVect = np.arange(0, X_train.shape[0], step = 1)

fig, ax = plt.subplots()
h = ax.hist2d(timeVect, y_train, cmap = "jet")

ax.set_xlabel("Numero evento", fontsize = 14)
ax.set_ylabel("Ground truth", fontsize = 14)

ax.set_yticks((0,1))


fig.colorbar(h[3], ax = ax)

plt.show()


#%% Definisco il classificatore

"""
classifier = KNeighborsClassifier(20, weights = "distance",
                                     algorithm = "auto", p = 2,
                                     n_jobs = -1)

"""

classifier = RandomForestClassifier(n_estimators = 30, criterion='entropy', 
                                    random_state = 0, n_jobs = -1)

#%% Addestro
t = time.time()
print("Che il training abbia inizio...")
classifier.fit(X_train_pruned, y_train_pruned)
print("Fine del training!")
print(f"Time elapsed: {time.time()-t:.2f")

#%% Predico
testEvts = None # None for all

t = time.time()

y_pred = classifier.predict(X_test_pruned[:testEvts,:])
#y_test_predicted_proba = KNN.predict_proba(X_test)

print(f"Time elapsed: {time.time()-t}")



#%% Scoring
cm = confusion_matrix(y_test[:testEvts], y_pred)
print("Confusion Matrix: ")
print(cm)

accuracy = accuracy_score(y_test[:testEvts], y_pred)
print(f"Print: Accuracy: {accuracy}")