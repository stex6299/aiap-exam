#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 08:30:14 2023

@author: steca
"""

# Allows debug on spyder in my spyder-cf environment
import collections
collections.Callable = collections.abc.Callable


#%% Import moduli
import numpy as np
from matplotlib import pyplot as plt
import h5py

import time, os, re, sys

from sklearn import utils
from sklearn import model_selection 
from sklearn import preprocessing
from sklearn import metrics

from sklearn.model_selection import train_test_split, ParameterGrid

# K-fold validation
from sklearn.model_selection import RepeatedStratifiedKFold

# PCA
from sklearn.decomposition import PCA

# Scoring
# from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
# from sklearn.metrics import roc_curve, auc
from sklearn import metrics

# Classificatori
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.neural_network import MLPClassifier



#%% Settings

prefisso = "NNArticolo"
resultFile = f"Results/{prefisso}.npz"

print(f"Benvenuto nel programma {prefisso}")
print(f"Gli output saranno salvati in {resultFile}")


# Se crasha, per riniziare da un punto civile
startGrid = 0
StartKF = 0

thrEV = .95     # Soglia Explained Variance per pruning componenti
drawPlot = False

if drawPlot:
    #%matplotlib inline
    pass
    
testEvts = None    # Numero di eventi del TeS da usare per il test (None = tutti)
n_jobs = -1        # Numero di processi paralleli (-1 = Numero di cores)

    
# K-fold
n_splits = 5    # Numero folder
n_repeats = 2   # Numero ripetizioni


# >>>
# Grid search
hidden_layer_sizes = [    np.array([ 300 for _ in range(5) ])        ]
activation = ['tanh']       
learning_rate_init = [0.05]         

# Dizionario per la grid search
paramDict = {'hidden_layer_sizes': hidden_layer_sizes,
              'activation': activation,
              'learning_rate_init': learning_rate_init}



# ROC Curve
nrocstep = 1000                            # Numero punti per curva ROC
xROC = np.linspace(0,1,nrocstep)           # Vettore su cui interpolare curva ROC


#%% Load dei dati
#os.chdir(r"G:\Corsi\AIAP - Artificial Intelligence for Astrophysical Problems\Esame\Repo")
dati = np.load("Dataset/dati.npy")
#dati.shape

dati = utils.shuffle(dati)
X = dati[:,1:]
y = dati[:,0]


#%% Verifico di aver shufflato [Copiato da 01]

# Indice degli eventi (0, 1, 2....)
timeVect = np.arange(0, X.shape[0], step = 1)

if drawPlot:
    fig, ax = plt.subplots()
    h = ax.hist2d(timeVect, y, cmap = "jet")
    
    ax.set_xlabel("Numero evento", fontsize = 14)
    ax.set_ylabel("Ground truth", fontsize = 14)
    
    ax.set_yticks((0,1))
    
    
    fig.colorbar(h[3], ax = ax)
    
    plt.show()


#%% Grid search

# Definisco la grid search
list_grid = list(ParameterGrid(paramDict))
nStepGrid = len(list_grid)
print(f"Saranno provate {nStepGrid} configurazioni di iper-parametri")





#%% Applico k-fold

"""
Faccio 5 folders in modo tale da usare il 20% come test set
Faccio 2 ripetizioni in modo tale da fare diei validazioni per ciascun 
dataset e per ciascuna configurazione di iper-parametri
"""
rskf = RepeatedStratifiedKFold(n_splits = n_splits, 
                               n_repeats = n_repeats, 
                               random_state = 13)

nStepKF = rskf.get_n_splits(X, y)
print(f"Saranno effettuati {nStepKF} step di k-fold per ciascuna configurazione di iper-parametri")



#%% Vettori globali di scoring

# Valori medi
globConfusionMatrix = np.zeros((2, 2, nStepGrid), dtype = float)

globAccuracy    = np.zeros(nStepGrid, dtype = float)
globPrecision   = np.zeros(nStepGrid, dtype = float)
globRecall      = np.zeros(nStepGrid, dtype = float)
globF1          = np.zeros(nStepGrid, dtype = float)

globxROC        = np.zeros((nrocstep, nStepGrid), dtype = float)
globyROC        = np.zeros((nrocstep, nStepGrid), dtype = float)
globAUC         = np.zeros(nStepGrid, dtype = float)




# Errori
globConfusionMatrixErr = np.zeros((2, 2, nStepGrid), dtype = float)

globAccuracyErr     = np.zeros(nStepGrid, dtype = float)
globPrecisionErr    = np.zeros(nStepGrid, dtype = float)
globRecallErr       = np.zeros(nStepGrid, dtype = float)
globF1Err           = np.zeros(nStepGrid, dtype = float)



globxROCErr         = np.zeros((nrocstep, nStepGrid), dtype = float)
globyROCErr         = np.zeros((nrocstep, nStepGrid), dtype = float)
globAUCErr          = np.zeros(nStepGrid, dtype = float)




# I valori ritornati da metrics.roc_curve li esporto come liste perché
# non capisco la logica con cui stabilisce quanti punti fare
globallstfpr = []
globallsttpr = []
globallstthresholds = []


# >>>
# Iper-parametri, li esporto anche come liste singole
iperParam_hidden_layer_sizes        = []
iperParam_activation                = []
iperParam_learning_rate_init        = []


#%% Big loop

T = time.time()

# Grid search: Ciclo su una certa combinazione di iper parametri
for j, grid_dict in enumerate(list_grid):
    
    # Condizione per iniziare in ritardo. In realtà, per come salvo, è "inutile"
    if j < startGrid: continue

    
    
    # >>>
    # Estraggo i parametri dello step corrente grid search
    tmp_hidden_layer_sizes = grid_dict['hidden_layer_sizes']
    tmp_activation = grid_dict['activation']
    tmp_learning_rate_init = grid_dict['learning_rate_init']
    
    # Li appendo a delle liste per poterli esportare
    iperParam_hidden_layer_sizes.append(tmp_hidden_layer_sizes)
    iperParam_activation.append(tmp_activation)
    iperParam_learning_rate_init.append(tmp_learning_rate_init)
    
    
    
    # Resetto i parametri per lo scoring che medierò sulle k-fold
    tmpConfusionMatrix = np.zeros((2, 2, nStepKF), dtype = float)
    
    tmpAccuracy     = np.zeros(nStepKF, dtype = float)
    tmpPrecision    = np.zeros(nStepKF, dtype = float)
    tmpRecall       = np.zeros(nStepKF, dtype = float)
    tmpF1           = np.zeros(nStepKF, dtype = float)
    
    
    
    # ROC Curves
    
    # Punti densi per calcolo aree
    tmpxROC = np.zeros((nrocstep, nStepKF), dtype = float)
    tmpyROC = np.zeros((nrocstep, nStepKF), dtype = float)
    
    # Esporto come liste anche i valori grezzi 
    # (liste perché non capisco che lunghezza hanno i vettori ritornati. 
    # Forse potrebbero essere non standard?)
    lstfpr = []
    lsttpr = []
    lstthresholds = []
    
    tmpAUC = np.zeros(nStepKF, dtype = float)
    
    
    
    
    # K-Fold: Itero su una certa combinazione di TrS/TeS
    for i, (train_index, test_index) in enumerate(rskf.split(X, y)):
        
        # Condizione per iniziare in ritardo. In realtà, per come salvo, è "inutile"
        if (j == startGrid) & (i < StartKF): continue
        
        print(f"\n\n\nStep grid Search: {j+1} / {nStepGrid}\tStep K-Fold: {i+1} / {nStepKF}")
        
        
        # Ottengo TrS e TeS usando le varie K-fold
        X_train = X[train_index,:]
        y_train = y[train_index]
        X_test = X[test_index,:]
        y_test = y[test_index]
        
        
        
        # *** Data Preparation ***
        
        # Applico scaling MinMax
        scaler = preprocessing.MinMaxScaler()
        scaler.fit(X_train)
        X_train_scaled = scaler.transform(X_train)


        # Applico la PCA
        pca = PCA()
        pca.fit(X_train_scaled)
        X_train_pca = pca.transform(X_train_scaled)


        # Estraggo le Explained Variances
        EV = pca.explained_variance_ratio_
        cumEV = np.cumsum(EV)


        # Stabilisco che componenti tenere per il pruning
        # Le componenti sono già ordinate
        idxLastFeature = np.sum(cumEV <= thrEV)
        print(f"Per avere il {thrEV * 100} % di Explaied Variance, sono necessarie {idxLastFeature} features ")


        # Pruno tenendo solo le prime componenti più informative
        X_train_pruned = X_train_pca[:,:idxLastFeature]     # Pruno




        # Estraggo info su Test Set
        X_test_scaled = scaler.transform(X_test)        # Riscalo
        X_test_pca = pca.transform(X_test_scaled)       # Trasformo PCA 
        X_test_pruned = X_test_pca[:,:idxLastFeature]   # Pruno



        # >>>
        # Definisco il classifier        
        classifier = MLPClassifier(hidden_layer_sizes = np.array([ 300 for _ in range(5) ]),
                                   activation = "tanh",
                                   learning_rate_init = .05,

                                   solver = "sgd", alpha = 0.0001,
				   batch_size = 100, momentum = .99999980000003999999, 
				   learning_rate = "invscaling", power_t = 1e-5, 
				   early_stopping = True, n_iter_no_change = 10, tol = 1e-5 )
        
        
        
        # *** Training ***
        
        t = time.time()
        print("\n-->Che il training abbia inizio...")
        classifier.fit(X_train_pruned, y_train)
        print(f"Time elapsed for training: {time.time()-t:.2f} s")
        
        
        
        
        # *** Prediction ***
        
        t = time.time()
        print("\n-->Che la predizione abbia inizio...")
        y_pred = classifier.predict(X_test_pruned[:testEvts,:])
        y_pred_proba = classifier.predict_proba(X_test_pruned[:testEvts,:])[:,1]
        print(f"Time elapsed for prediction: {time.time()-t:.2f} s")
        
        
        
        
        # *** Scoring ***
        
        confusionMatrix =   metrics.confusion_matrix(y_test[:testEvts], y_pred)
        accuracy        =   metrics.accuracy_score(y_test[:testEvts], y_pred)
        precision       =   metrics.precision_score(y_test[:testEvts], y_pred)
        recall          =   metrics.recall_score(y_test[:testEvts], y_pred)
        f1              =   metrics.f1_score(y_test[:testEvts], y_pred)
        
        
        
        # Salvo gli scoring parziali
        tmpConfusionMatrix[:,:,i] = confusionMatrix
        tmpAccuracy[i] = accuracy
        tmpPrecision[i] = precision
        tmpRecall[i] = recall
        tmpF1[i] = f1
        
        
        
        
        
        # *** ROC Curve ***
        fpr, tpr, thresholds = metrics.roc_curve(y_test, y_pred_proba)
        
        # Creo un'interpolazione denza per miglior calcolo aree
        yROC = np.interp(xROC, fpr, tpr)
        #myAuc = metrics.auc(fpr, tpr)
        myAUC = metrics.auc(xROC, yROC)
        
        
        # Salvo i valori
        tmpxROC[:,i] = xROC
        tmpyROC[:,i] = yROC
        
        
        # 
        lstfpr.append(fpr)
        lsttpr.append(tpr)
        lstthresholds.append(thresholds)
        
        
        
        tmpAUC[i] = myAUC
        
        
        
        
        
        
        # Some output
        print(f"\nResults:")
        print("Confusion Matrix: ")
        print(confusionMatrix)

        print(f"Print: Accuracy: {accuracy:.2f}")
        print(f"Print: precision: {precision:.2f}")
        print(f"Print: recall: {recall:.2f}")
        print(f"Print: f1: {f1:.2f}")

        print(f"Print: AUC: {myAUC:.2f}")


        
        
        # // TODO
        # Salvo i dati dello step
        
        #opts = {"compression":"gzip", "chunks":True}
        # opts = {}
        # with h5py.File(resultFile, 'a', libver='latest') as hf:
            
        #     print(f"Sto per appendere {resultFile}")
        #     hf.swmr_mode = True
            
        #     if (len(hf.keys())) == 0:
        #         hf.create_dataset("n_estimators", data =  step_n_estimators, maxshape=(None,), **opts)
        #         hf.create_dataset("criterion", data =  step_criterion, maxshape=(None,), **opts)
        #         hf.create_dataset("min_samples_leaf", data =  step_min_samples_leaf, maxshape=(None,), **opts)
                
        #         hf.create_dataset("confusionMatrix", data =  confusionMatrix, maxshape=(None,2,2), **opts)
                
        #         hf.create_dataset("accuracy", data =  accuracy, maxshape=(None,2,2), **opts)
        #         hf.create_dataset("precision", data =  precision, maxshape=(None,), **opts)
        #         hf.create_dataset("recall", data =  recall, maxshape=(None,), **opts)
        #         hf.create_dataset("f1", data =  f1, maxshape=(None,), **opts)
                
        #         hf.create_dataset("xROC", data =  xROC, maxshape=(None,nrocstep), **opts)
        #         hf.create_dataset("yROC", data =  yROC, maxshape=(None,nrocstep), **opts)
        #         hf.create_dataset("myAUC", data =  myAUC, maxshape=(None,), **opts)
        #     else:
                
                
        #         tmpMin = np.nan

        #         for k in hf.keys():
        #             if np.isnan(tmpMin):
        #                 tmpMin = hf[k].shape[0]
        #             else:
        #                 if hf[k].shape[0] < tmpMin:
        #                     tmpMin = hf[k].shape[0]
                            
        #         for k in hf.keys():
        #             hf[k].resize((tmpMin + 1), axis = 0)
                    
        #         hf["n_estimators"][-1:] = step_n_estimators
        #         hf["criterion"][-1:] = step_criterion
        #         hf["min_samples_leaf"][-1:] = step_min_samples_leaf
                    
        #         hf["confusionMatrix"][-1:] = confusionMatrix
                
        #         hf["accuracy"][-1:] = accuracy
        #         hf["precision"][-1:] = precision
        #         hf["recall"][-1:] = recall
        #         hf["f1"][-1:] = f1

        #         hf["xROC"][-1:] = xROC
        #         hf["yROC"][-1:] = yROC
        #         hf["myAUC"][-1:] = myAUC

            
            
        # print(classifier.get_params())
        # print(grid_dict)
        
        
        
    # *** MEDIE SU K-FOLD ***

    # Medio le grandezze di scoring sulle varie k-fold        
    meanConfusionMatrix = np.mean(tmpConfusionMatrix, axis = 2)
    
    meanAccuracy = np.mean(tmpAccuracy)
    meanPrecision = np.mean(tmpPrecision)
    meanRecall = np.mean(tmpRecall)
    meanF1 = np.mean(tmpF1)
    
    
    # Medio le curve ROC
    meanxROC = np.mean(tmpxROC, axis = 1)
    meanyROC = np.mean(tmpyROC, axis = 1)
    meanAUC = np.mean(tmpAUC)
    
        
    
    # *** ERRORI SU K-FOLD ***
    
    # Scoring
    stdConfusionMatrix = np.std(tmpConfusionMatrix, axis = 2) / np.sqrt(nStepKF - 1)
    
    stdAccuracy = np.std(tmpAccuracy) / np.sqrt(nStepKF - 1)
    stdPrecision = np.std(tmpPrecision) / np.sqrt(nStepKF - 1)
    stdRecall = np.std(tmpRecall) / np.sqrt(nStepKF - 1)
    stdF1 = np.std(tmpF1) / np.sqrt(nStepKF - 1)
    
    
    # ROC
    stdxROC = np.std(tmpxROC, axis = 1) / np.sqrt(nStepKF - 1)
    stdyROC = np.std(tmpyROC, axis = 1) / np.sqrt(nStepKF - 1)
    stdAUC = np.std(tmpAUC) / np.sqrt(nStepKF - 1)
    
    
    
    # *** Appendo ai vettori globali ***
    
    # Medie
    globConfusionMatrix[:,:,j] = meanConfusionMatrix
    
    globAccuracy[j] = meanAccuracy
    globPrecision[j] = meanPrecision
    globRecall[j] = meanRecall
    globF1[j] = meanF1
    
    globxROC[:,j] = meanxROC
    globyROC[:,j] = meanyROC
    globAUC[j] = meanAUC
    
    
    # Err
    globConfusionMatrixErr[:,:,j] = stdConfusionMatrix
    
    globAccuracyErr[j] = stdAccuracy
    globPrecisionErr[j] = stdPrecision
    globRecallErr[j] = stdRecall
    globF1Err[j] = stdF1
    
    globxROCErr[:,j] = stdxROC
    globyROCErr[:,j] = stdyROC
    globAUCErr[j] = stdAUC
    
    
    
    
    # Esporto anche i valori grezzi dati dalle ROC curves, just in case
    globallstfpr.append(lstfpr)
    globallsttpr.append(lsttpr)
    globallstthresholds.append(lstthresholds)
    
    
    
    # >>>
    # Scrivo in un file di testo per comodità
    print("\nSto per dumpare nel file di testo...")
    with open(resultFile.replace("npz", "txt"), "a") as f:
        if j == 0:
            f.write("hidden_layer_sizes\tactivation\tlearning_rate_init\tmeanConfusionMatrix\tmeanAccuracy\tmeanPrecision\tmeanRecall\tmeanF1\tmeanAUC\n")
        f.write(f"{tmp_hidden_layer_sizes}\t{tmp_activation}\t{tmp_learning_rate_init}\t{meanConfusionMatrix.reshape((1,4))}\t{meanAccuracy:.2f}\t{meanPrecision:.2f}\t{meanRecall:.2f}\t{meanF1:.2f}\t{meanAUC}\n")
        #f.write(f"{meanxROC}\n")
        #f.write(f"{meanyROC}\n")
    print("...Ho dumpato\n")
        



print(f"\n\nFine del big loop. Time elapsed {time.time()-T:.2f} s")


#%% Save data
print(f"Sto per salvare {resultFile}")

# >>>
# Salvo le grandezze mediate sulle ripetizioni dei Kfold, con rispettivi errori
np.savez(resultFile, globConfusionMatrix = globConfusionMatrix,
                     globAccuracy = globAccuracy,
                     globPrecision = globPrecision,
                     globRecall = globRecall,
                     globF1 = globF1,
                     
                     globxROC = globxROC,
                     globyROC = globyROC,
                     globAUC = globAUC,
                     
                     globConfusionMatrixErr = globConfusionMatrixErr,
                     globAccuracyErr = globAccuracyErr,
                     globPrecisionErr = globPrecisionErr,
                     globRecallErr = globRecallErr,
                     globF1Err = globF1Err,
                     
                     globxROCErr = globxROCErr,
                     globyROCErr = globyROCErr,
                     globAUCErr = globAUCErr,
                     
                     
                     globallstfpr = globallstfpr,
                     globallsttpr = globallsttpr,
                     globallstthresholds = globallstthresholds,
                     
                     list_grid = list_grid,
                     iperParam_hidden_layer_sizes = iperParam_hidden_layer_sizes,
                     iperParam_activation = iperParam_activation, 
                     iperParam_learning_rate_init = iperParam_learning_rate_init
                     )

print(f"Ho salvato {resultFile}")
print("Bye bye")
