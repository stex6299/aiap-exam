# THIS REPO IS NO LONGER SUPPORTED
# I HAVE MOVED TO THE NEW ONE, WITH A NEW DATASET
[https://gitlab.com/stex6299/aiap-exam-2/](https://gitlab.com/stex6299/aiap-exam-2/)


# What's here
Repository dove raccolgo gli script per l'esame del corso __Artificial Intelligence for Astrophisical Problems__

# Files
Sui seguenti scripts ho fatto una grid search. I risultati sono in [Results](./Results)
- [10_RandomForest.py](./10_RandomForest.py): Random forest, testato sia con soglia a 95% che a 99% sulla Explained Variance per pruning delle features
- [20_Adaboost.py](./20_Adaboost.py): provato solo con soglia a 95%
- [30_SVC](./30_SVC): prova di una support vector: abbandonata, non è lo strumento adatto ai nostri scopi
